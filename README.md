# Context

This repository contains the BCARE model for my final paper @ Unisinos. This model consists in a SaaS application that receive data from clients, organize the information and storage on the database. Also, when the procedures is finished, the model turn on the prediction algorithms to predict if the user will be stressed or not. This project was built on Java SE 16, running on Wildfly 20, with PostgreSQL 13.2. All the communication is realized by endpoints on HTTP protocol and JSON format. The project also use Maven for managing the the dependencies.

This project uses the BCARE Simulator as client that can be accessed by: https://bitbucket.org/brunoqmt/bcare-simulator

# Tooling

- **Weka**: The project makes use of Weka Java Library for Machine Learning.
- **PostgreSQL**: This project use PostgreSQL Database.
- **Java**: As the main language.
- **Eclipse IDE**: As the IDE to develop the model.
- **JSON**: As the format of communication.
- **UTF-8**: As the encoding.
- **Wildfly**: As the Application Server to host the model.


# Machine Learning

The project uses 3 different techniques to the machine learning:

- **Regression Logistic**
- **Naive Bayes**
- **C4.5 as J48 on Weka**


# Architecture Detail Diagram

![01_Architecture_Detail](https://drive.google.com/uc?export=view&id=1qs9DRT79KccZQ_G-86HMibBD5M4lGCj-)


# Technologies

![02_Technologies_Detail](https://drive.google.com/uc?export=view&id=16IGsjhZLOyO6xvLqUv81Liu6dgIRempC)


# ER Diagram

![03_ER_Diagram](https://drive.google.com/uc?export=view&id=1Ik8t-bCJWH3FSm4o1rzHdLrvp9Wp8J4W)


# Precision Results

![04_Results](https://drive.google.com/uc?export=view&id=1uKAJo5DYZzg25s6O1qx9ZR7FifozxUEw)