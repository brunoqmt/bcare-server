package br.unisinos.bcare.wekatests;

import weka.classifiers.bayes.NaiveBayes;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

/**
 * 
 * @author Bruno Mota Alves <bruno.alves@multi24h.com.br>
 * @since  15 de out. de 2021
 */
public class WekaTestSample {
	
	public static void main(String[] args) throws Exception {
		DataSource ds = new DataSource("C:\\Users\\Mota\\Desktop\\vendas.arff");
		Instances ins = ds.getDataSet();
		
		ins.setClassIndex(3);
		
		NaiveBayes nb = new NaiveBayes();
		nb.buildClassifier(ins);
		
		Instance novo = new DenseInstance(4);
		novo.setDataset(ins);
		novo.setValue(0, "F");
		novo.setValue(1, "20-39");
		novo.setValue(2, "Sim");
		
		double probabilidade[] = nb.distributionForInstance(novo);
		System.out.println("Sim: " + probabilidade[1]);
		System.out.println("Não: " + probabilidade[0]);
		
	}

}
