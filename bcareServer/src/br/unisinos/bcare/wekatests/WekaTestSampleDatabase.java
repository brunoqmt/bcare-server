package br.unisinos.bcare.wekatests;

import java.io.File;
import java.io.IOException;

import weka.classifiers.bayes.NaiveBayes;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;
import weka.experiment.InstanceQuery;

/**
 * 
 * @author Bruno Mota Alves <bruno.alves@multi24h.com.br>
 * @since 15 de out. de 2021
 */
public class WekaTestSampleDatabase {

	public File readFile(String path) {
		ClassLoader classLoader = getClass().getClassLoader();
		File fileProps = new File(classLoader.getResource(path).getFile());

		return fileProps;
	}

	public static void main(String[] args) throws Exception {
		try {
			/* Another way to do it */
			/*
			 * DatabaseLoader loader = new DatabaseLoader();
			 * loader.setSource("jdbc:postgresql://localhost:5432/bcare-server", "postgres",
			 * "root");s
			 * loader.setQuery("SELECT sexo, idade, filhos, gasta_muito FROM vendas");
			 * Instances data = loader.getDataSet(); data.setClassIndex(data.numAttributes()
			 * - 1);
			 */

			File file = new WekaTestSampleDatabase().readFile("DatabaseUtils.props");
			System.out.println(file.exists());

			InstanceQuery query = new InstanceQuery();
			query.setCustomPropsFile(file);
			query.setDatabaseURL("jdbc:postgresql://localhost:5432/bcare-server");
			query.setUsername("postgres");
			query.setPassword("root");
			query.setQuery("SELECT sexo, idade, filhos, gasta_muito FROM vendas");
			Instances data = query.retrieveInstances();
			System.out.println(data);
			
			
			
			/* Requentar anterior */
			
			
			data.setClassIndex(3);
			
			NaiveBayes nb = new NaiveBayes();
			nb.buildClassifier(data);
			
			Instance novo = new DenseInstance(4);
			novo.setDataset(data);
			novo.setValue(0, "F");
			novo.setValue(1, "20-39");
			novo.setValue(2, "Sim");
			
			double probabilidade[] = nb.distributionForInstance(novo);
			System.out.println("Sim: " + probabilidade[1]);
			System.out.println("Não: " + probabilidade[0]);
			

		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
