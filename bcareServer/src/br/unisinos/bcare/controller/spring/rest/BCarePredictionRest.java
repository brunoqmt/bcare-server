package br.unisinos.bcare.controller.spring.rest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import br.unisinos.bcare.controller.TrailController;
import br.unisinos.bcare.model.bean.Trail;

/**
 * 
 * @author Bruno Mota Alves <bruno.alves@multi24h.com.br>
 * @since 26 de ago. de 2021
 */
@Controller
public class BCarePredictionRest {
	
	private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

	@RequestMapping(value = "/teste/bruno", method = RequestMethod.POST, consumes = "application/json;charset=UTF-8", produces = "application/json;charset=UTF-8")
	public @ResponseBody String testeBruno(@RequestHeader("teste") String teste, @RequestBody() String jsonContent) {
		return "Retorno de teste: " + teste + "  |  " + jsonContent;
	}

	@RequestMapping(value = "/nb/persistAndPrediction", method = RequestMethod.GET, consumes = "application/json;charset=UTF-8", produces = "application/json;charset=UTF-8")
	public @ResponseBody int getPrediction(@RequestHeader("trail") String trailResponse) {
		Trail trail = gson.fromJson(trailResponse, Trail.class);
		int stressStatus = new TrailController().manageRequest(trail);
		return stressStatus;
	}

}
