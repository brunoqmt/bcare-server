package br.unisinos.bcare.controller;

import java.sql.Connection;

import br.unisinos.bcare.controller.weka.J48WekaController;
import br.unisinos.bcare.controller.weka.LogisticWekaController;
import br.unisinos.bcare.controller.weka.NaiveBayesWekaController;
import br.unisinos.bcare.model.bean.RatePlaceStress;
import br.unisinos.bcare.model.bean.Trail;
import br.unisinos.bcare.model.bean.TrailPrediction;
import br.unisinos.bcare.model.dao.ConnectionDAO;
import br.unisinos.bcare.model.dao.TrailDAO;
import br.unisinos.bcare.model.dao.TrailPredictionDAO;

/**
 * 
 * @author Bruno Mota Alves <bruno.alves@multi24h.com.br>
 * @since 17 de out. de 2021
 */
public class TrailController {

	private Connection connection;
	private TrailDAO trailDAO;
	private TrailPredictionDAO trailPredictionDAO;
//	private WekaController wekaController;
	private SecondaryPredictionController secPredController;

	public TrailController() {
		connection = new ConnectionDAO().connect();

		trailDAO = new TrailDAO(connection);
		trailPredictionDAO = new TrailPredictionDAO(connection);
//		wekaController = new WekaController();
		secPredController = new SecondaryPredictionController(connection);
	}

	public TrailController(Connection connection) {
		this.connection = connection;
		trailDAO = new TrailDAO(connection);
//		wekaController = new WekaController();
	}

	public Trail persistTrail(Trail trail) {
		/* Persist the trail */
		return trailDAO.insert(trail);
	}

	public int manageRequest(Trail trail) {
		try {
			int stressStatus = 0;
			/* Verify wich module of prediction must be used */
			Boolean trailHistory = trailDAO.existsTrailHistory(trail);
			/* Persist the Trail X Prediction */
			TrailPrediction trailPrediction = new TrailPrediction();

			if (trailHistory) {
				/* Persist Trail */
				trail = this.persistTrail(trail);

				/* Call the prediction for algorithms */
				this.managePredictionNaiveBayes(trail, trailPrediction);
				/* Persist Trail Prediction */
				trailPredictionDAO.insert(trailPrediction);
				
				this.managePredictionLogistic(trail, trailPrediction);
				/* Persist Trail Prediction */
				trailPredictionDAO.insert(trailPrediction);
				
				this.managePredictionJ48(trail, trailPrediction);
				/* Persist Trail Prediction */
				trailPredictionDAO.insert(trailPrediction);
				
			} else {
				WekaController wekaController = new WekaController();
				/* Build the Rate of Places */
				secPredController.buildListPlaceStress();
				RatePlaceStress placeOfStress = secPredController.findPlaceByID(trail.getPlace());

				/* Persist Trail */
				trail = this.persistTrail(trail);

				/* Get the Status Stress for prediction */
				stressStatus = wekaController.chooseProbabilitySecondModule(placeOfStress.getRatio());

				// PASSAR OS DADOS PARA O ELENCADOR DE ESTRESSE, RETORNAR O STATUS E PREENCHER O
				// PERCENTUAL

				if (stressStatus == 0) {
					/* Some fail */
					System.out.println("Fail with the return of Stress Status");
				} else {
					trailPrediction.setIdTrail(trail.getId());
					trailPrediction.setIdAlgorithm(4);
					trailPrediction.setRelaxedProbability(0.0);
					trailPrediction.setNormalProbability(1.0 - placeOfStress.getRatio());
					trailPrediction.setStressedProbability(placeOfStress.getRatio());
					trailPrediction.setStatusPrediction(stressStatus);
					trailPrediction.setPredictedStress(true);
					trailPrediction.setSourceModulePrediction(2);
					if (stressStatus == trail.getStressCondition().getStatusCondition()) {
						trailPrediction.setPredictionWasCorrect(true);
					} else {
						trailPrediction.setPredictionWasCorrect(false);
					}
				}
				/* Persist Trail Prediction */
				trailPredictionDAO.insert(trailPrediction);

			}


			/* Connection close */
			new ConnectionDAO().disconnect(connection);

			return stressStatus;
		} catch (Exception e) {
			new ConnectionDAO().disconnect(connection);
			e.printStackTrace();
			return 0;
		}
	}

	/**
	 * Created for reuse purposes. Only for validation and multiples algorithms.
	 * 
	 * @param trail
	 * @param trailPrediction
	 */
	private void managePredictionNaiveBayes(Trail trail, TrailPrediction trailPrediction) {
		NaiveBayesWekaController wekaController = new NaiveBayesWekaController(); 
		int stressStatus = 0;
		
		/* Call the prediction */
		double[] probabilities = wekaController.predictionProcess(trail);

		/* Get the Status Stress for prediction */
		stressStatus = wekaController.chooseProbabilityFirstModule(probabilities);

		if (stressStatus == 0) {
			/* Some fail */
			System.out.println("Fail with the return of Stress Status");
		} else {
			trailPrediction.setIdTrail(trail.getId());
			trailPrediction.setIdAlgorithm(1);
			trailPrediction.setRelaxedProbability(0.0);
			trailPrediction.setNormalProbability(probabilities[0]);
			trailPrediction.setStressedProbability(probabilities[1]);
			trailPrediction.setStatusPrediction(stressStatus);
			trailPrediction.setPredictedStress(true);
			trailPrediction.setSourceModulePrediction(1);
			if (stressStatus == trail.getStressCondition().getStatusCondition()) {
				trailPrediction.setPredictionWasCorrect(true);
			} else {
				trailPrediction.setPredictionWasCorrect(false);
			}
		}
	}
	
	/**
	 * Created for reuse purposes. Only for validation and multiples algorithms.
	 * 
	 * @param trail
	 * @param trailPrediction
	 */
	private void managePredictionLogistic(Trail trail, TrailPrediction trailPrediction) {
		LogisticWekaController wekaController = new LogisticWekaController(); 
		int stressStatus = 0;
		
		/* Call the prediction */
		double[] probabilities = wekaController.predictionProcess(trail);

		/* Get the Status Stress for prediction */
		stressStatus = wekaController.chooseProbabilityFirstModule(probabilities);

		if (stressStatus == 0) {
			/* Some fail */
			System.out.println("Fail with the return of Stress Status");
		} else {
			trailPrediction.setIdTrail(trail.getId());
			trailPrediction.setIdAlgorithm(2);
			trailPrediction.setRelaxedProbability(0.0);
			trailPrediction.setNormalProbability(probabilities[0]);
			trailPrediction.setStressedProbability(probabilities[1]);
			trailPrediction.setStatusPrediction(stressStatus);
			trailPrediction.setPredictedStress(true);
			trailPrediction.setSourceModulePrediction(1);
			if (stressStatus == trail.getStressCondition().getStatusCondition()) {
				trailPrediction.setPredictionWasCorrect(true);
			} else {
				trailPrediction.setPredictionWasCorrect(false);
			}
		}
	}
	
	/**
	 * Created for reuse purposes. Only for validation and multiples algorithms.
	 * 
	 * @param trail
	 * @param trailPrediction
	 */
	private void managePredictionJ48(Trail trail, TrailPrediction trailPrediction) {
		J48WekaController wekaController = new J48WekaController(); 
		int stressStatus = 0;
		
		/* Call the prediction */
		double[] probabilities = wekaController.predictionProcess(trail);

		/* Get the Status Stress for prediction */
		stressStatus = wekaController.chooseProbabilityFirstModule(probabilities);

		if (stressStatus == 0) {
			/* Some fail */
			System.out.println("Fail with the return of Stress Status");
		} else {
			trailPrediction.setIdTrail(trail.getId());
			trailPrediction.setIdAlgorithm(3);
			trailPrediction.setRelaxedProbability(0.0);
			trailPrediction.setNormalProbability(probabilities[0]);
			trailPrediction.setStressedProbability(probabilities[1]);
			trailPrediction.setStatusPrediction(stressStatus);
			trailPrediction.setPredictedStress(true);
			trailPrediction.setSourceModulePrediction(1);
			if (stressStatus == trail.getStressCondition().getStatusCondition()) {
				trailPrediction.setPredictionWasCorrect(true);
			} else {
				trailPrediction.setPredictionWasCorrect(false);
			}
		}
	}

}
