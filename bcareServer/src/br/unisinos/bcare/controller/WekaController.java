package br.unisinos.bcare.controller;

import java.io.File;
import java.io.IOException;

import br.unisinos.bcare.model.bean.Trail;
import br.unisinos.bcare.model.bean.User;
import br.unisinos.bcare.model.config.spring.ApplicationContextProvider;
import weka.classifiers.bayes.NaiveBayes;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;
import weka.experiment.InstanceQuery;

/**
 * 
 * @author Bruno Mota Alves <bruno.alves@multi24h.com.br>
 * @since 15 de out. de 2021
 */
public class WekaController {

	private NaiveBayes nb;
//	private Logistic nb;
//	private J48 nb;
	private Instances dataset;

	private File readFile(String path) {
		ClassLoader classLoader = getClass().getClassLoader();
		File fileProps = new File(classLoader.getResource(path).getFile());
		return fileProps;
	}

	private void rebuildProfileML(User user) throws Exception {
		File props = ApplicationContextProvider.getApplicationContext().getResource("DatabaseUtils.props").getFile();
//		File props = readFile("DatabaseUtils.props");
		InstanceQuery query = new InstanceQuery();
		query.setCustomPropsFile(props);
		query.setDatabaseURL("jdbc:postgresql://localhost:5432/bcare");
		query.setUsername("postgres");
		query.setPassword("root");
		query.setQuery(
				"SELECT t.hour, t.day_of_week, p.description, sc.status_condition_text FROM trails t JOIN users u ON u.id = t.id_user JOIN places p ON p.id = t.id_place JOIN stress_condition sc ON sc.id = t.id_stress_condition WHERE u.id = "
						+ user.getId());
		dataset = query.retrieveInstances();
		dataset.setClassIndex(3);
//		System.out.println(dataset.attribute(3));

//		System.out.println(dataset);
		nb = new NaiveBayes();
//		nb = new Logistic();
//		nb = new J48();
		nb.buildClassifier(dataset);
	}

	private double[] getProbability(Instance instance) throws Exception {
		/* Connect to database and rebuild profile */
		double probability[] = nb.distributionForInstance(instance);
		return probability;
	}

	private Instance buildInstance(String hour, String dayOfWeek, String location) {
		Instance newInstance = new DenseInstance(4);
		newInstance.setDataset(dataset);
		newInstance.setValue(0, hour);
		newInstance.setValue(1, dayOfWeek);
		newInstance.setValue(2, location);
		return newInstance;
	}

	public double[] predictionProcess(Trail trail) {
		try {
			WekaController weka = new WekaController();
			weka.rebuildProfileML(trail.getUser());
			Instance newInstance = weka.buildInstance(trail.getHour(), trail.getDayOfWeek(), trail.getPlace().getDescription());
			double[] probabilities = weka.getProbability(newInstance);
			// deal with the return and classify
			return probabilities;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public int chooseProbabilityFirstModule(double[] probabilities) {
		/* Get the status_condition_text */
		double current = 0.0;
		int stressStatus = 0;
		if (probabilities[0] < 1.0 && probabilities[0] > current) {
			/* Normal */
			current = probabilities[0];
			stressStatus = 2;
		}
		if (probabilities[1] < 1.0 && probabilities[1] > current) {
			/* Stressed */
			current = probabilities[1];
			stressStatus = 1;
		}
		return stressStatus;
	}

	public int chooseProbabilitySecondModule(Double ratio) {
		/* Get the status_condition_text */
		int stressStatus = 0;
		if (ratio <= 0.5) {
			/* Normal */
			stressStatus = 1;
		} else {
			/* Stressed */
			stressStatus = 2;
		}
		return stressStatus;
	}

	public static void main(String[] args) throws Exception {
		try {
			User user = new User();
			user.setId(1);

			WekaController weka = new WekaController();
			weka.rebuildProfileML(user);
//			Instance newInstance = weka.buildInstance("7", "2", "Trabalho 1");
//			double probability[] = weka.getProbability(newInstance);

//			System.out.println("Relaxado: " + probability[0]);
//			System.out.println("Normal: " + probability[1]);
//			System.out.println("Estressado: " + probability[2]);
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}