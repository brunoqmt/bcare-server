package br.unisinos.bcare.controller.weka;

import java.io.File;

import br.unisinos.bcare.model.bean.Trail;
import br.unisinos.bcare.model.bean.User;
import br.unisinos.bcare.model.config.spring.ApplicationContextProvider;
import weka.classifiers.bayes.NaiveBayes;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;
import weka.experiment.InstanceQuery;

/**
 * 
 * @author Bruno Mota Alves <bruno.alves@multi24h.com.br>
 * @since 15 de out. de 2021
 */
public class NaiveBayesWekaController {

	private NaiveBayes nb;
	private Instances dataset;

	private File readFile(String path) {
		ClassLoader classLoader = getClass().getClassLoader();
		File fileProps = new File(classLoader.getResource(path).getFile());
		return fileProps;
	}

	private void rebuildProfileML(User user) throws Exception {
		File props = ApplicationContextProvider.getApplicationContext().getResource("DatabaseUtils.props").getFile();
		InstanceQuery query = new InstanceQuery();
		query.setCustomPropsFile(props);
		query.setDatabaseURL("jdbc:postgresql://localhost:5432/bcare");
		query.setUsername("postgres");
		query.setPassword("root");
		query.setQuery("SELECT t.hour, t.day_of_week, p.description, sc.status_condition_text FROM trails t JOIN users u ON u.id = t.id_user JOIN places p ON p.id = t.id_place JOIN stress_condition sc ON sc.id = t.id_stress_condition WHERE u.id = "
				+ user.getId() + " "
						+ "AND t.datetime_moment >= '2017-01-01 00:00:00' AND t.datetime_moment <= '2020-12-31 23:59:59'"
						+ " ORDER BY sc.status_condition_text ASC");
		dataset = query.retrieveInstances();
		dataset.setClassIndex(3);
		nb = new NaiveBayes();
		nb.buildClassifier(dataset);
	}

	private double[] getProbability(Instance instance) throws Exception {
		/* Connect to database and rebuild profile */
		double probability[] = nb.distributionForInstance(instance);
		return probability;
	}

	private Instance buildInstance(String hour, String dayOfWeek, String location) {
		Instance newInstance = new DenseInstance(4);
		newInstance.setDataset(dataset);
		newInstance.setValue(0, hour);
		newInstance.setValue(1, dayOfWeek);
		newInstance.setValue(2, location);
		return newInstance;
	}

	public double[] predictionProcess(Trail trail) {
		try {
			NaiveBayesWekaController weka = new NaiveBayesWekaController();
			weka.rebuildProfileML(trail.getUser());
			Instance newInstance = weka.buildInstance(trail.getHour(), trail.getDayOfWeek(), trail.getPlace().getDescription());
			double[] probabilities = weka.getProbability(newInstance);
			// deal with the return and classify
			return probabilities;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public int chooseProbabilityFirstModule(double[] probabilities) {
		/* Get the status_condition_text */
		double current = 0.0;
		int stressStatus = 0;
		if (probabilities[0] < 1.0 && probabilities[0] > current) {
			/* Normal */
//			System.out.println("prob 0 normal: " + probabilities[0]);
			current = probabilities[0];
			stressStatus = 1;
		}
		if (probabilities[1] < 1.0 && probabilities[1] > current) {
			/* Stressed */
//			System.out.println("prob 1 normal: " + probabilities[1]);
			current = probabilities[1];
			stressStatus = 2;
		}
		return stressStatus;
	}

	public int chooseProbabilitySecondModule(Double ratio) {
		/* Get the status_condition_text */
		int stressStatus = 0;
		if (ratio <= 0.5) {
			/* Normal */
			stressStatus = 1;
		} else {
			/* Stressed */
			stressStatus = 2;
		}
		return stressStatus;
	}
}