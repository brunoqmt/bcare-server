package br.unisinos.bcare.controller;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import br.unisinos.bcare.model.bean.Place;
import br.unisinos.bcare.model.bean.RatePlaceStress;
import br.unisinos.bcare.model.dao.PlaceDAO;
import br.unisinos.bcare.model.dao.TrailDAO;

/**
 * 
 * @author Bruno Mota Alves <bruno.alves@multi24h.com.br>
 * @since 19 de out. de 2021
 */
public class SecondaryPredictionController {

//	private Connection connection;
	private TrailDAO trailDAO;
	private PlaceDAO placeDAO;
	private List<RatePlaceStress> data;

	public SecondaryPredictionController(Connection connection) {
//		this.connection = connection;
		trailDAO = new TrailDAO(connection);
		placeDAO = new PlaceDAO(connection);

	}
	
	public RatePlaceStress findPlaceByID(Place place ) {
		for (RatePlaceStress ratePlaceStress : data) {
			if(ratePlaceStress.getPlace().getId() == place.getId()) {
				return ratePlaceStress;
			}
		}
		return null;
	}

	public List<RatePlaceStress> buildListPlaceStress() {
		data = new ArrayList<>();

		/* Retrieve all the places */
		List<Place> places = placeDAO.retrieveAll();

		for (Place place : places) {
			Integer totalOccurrences = trailDAO.retrieveCountTrailOccurrences(place.getId());
			Integer stressOccurrences = trailDAO.retrieveCountStressOccurrences(place.getId(), 4);

			data.add(new RatePlaceStress(place, totalOccurrences, stressOccurrences));
		}

		return data;
	}
}
