package br.unisinos.bcare.model.bean;

/**
 * 
 * @author Bruno Mota Alves <bruno.alves@multi24h.com.br>
 * @since 5 de out. de 2021
 */
public class Place {

	private Integer id;
	private String description;
	private Double latitude;
	private Double longitude;
	private Double coefficient;
	private CategoryTime categoryTime = new CategoryTime();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getCoefficient() {
		return coefficient;
	}

	public void setCoefficient(Double coefficient) {
		this.coefficient = coefficient;
	}

	public CategoryTime getCategoryTime() {
		return categoryTime;
	}

	public void setCategoryTime(CategoryTime categoryTime) {
		this.categoryTime = categoryTime;
	}

}
