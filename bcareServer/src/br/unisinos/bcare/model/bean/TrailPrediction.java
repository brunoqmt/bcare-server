package br.unisinos.bcare.model.bean;

/**
 * 
 * @author Bruno Mota Alves <bruno.alves@multi24h.com.br>
 * @since 17 de out. de 2021
 */
public class TrailPrediction {

	private Integer id;
	private Integer idTrail;
	private Integer idAlgorithm;
	private Double relaxedProbability;
	private Double normalProbability;
	private Double stressedProbability;
	private Integer statusPrediction;
	private Boolean predictedStress;
	private Boolean predictionWasCorrect;
	private Integer sourceModulePrediction;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIdTrail() {
		return idTrail;
	}

	public void setIdTrail(Integer idTrail) {
		this.idTrail = idTrail;
	}

	public Integer getIdAlgorithm() {
		return idAlgorithm;
	}

	public void setIdAlgorithm(Integer idAlgorithm) {
		this.idAlgorithm = idAlgorithm;
	}

	public Double getRelaxedProbability() {
		return relaxedProbability;
	}

	public void setRelaxedProbability(Double relaxedProbability) {
		this.relaxedProbability = relaxedProbability;
	}

	public Double getNormalProbability() {
		return normalProbability;
	}

	public void setNormalProbability(Double normalProbability) {
		this.normalProbability = normalProbability;
	}

	public Double getStressedProbability() {
		return stressedProbability;
	}

	public void setStressedProbability(Double stressedProbability) {
		this.stressedProbability = stressedProbability;
	}

	public Integer getStatusPrediction() {
		return statusPrediction;
	}

	public void setStatusPrediction(Integer statusPrediction) {
		this.statusPrediction = statusPrediction;
	}

	public Boolean getPredictedStress() {
		return predictedStress;
	}

	public void setPredictedStress(Boolean predictedStress) {
		this.predictedStress = predictedStress;
	}

	public Boolean getPredictionWasCorrect() {
		return predictionWasCorrect;
	}

	public void setPredictionWasCorrect(Boolean predictionWasCorrect) {
		this.predictionWasCorrect = predictionWasCorrect;
	}

	public Integer getSourceModulePrediction() {
		return sourceModulePrediction;
	}

	public void setSourceModulePrediction(Integer sourceModulePrediction) {
		this.sourceModulePrediction = sourceModulePrediction;
	}

}
