package br.unisinos.bcare.model.bean;

/**
 * 
 * @author Bruno Mota Alves <bruno.alves@multi24h.com.br>
 * @since 19 de out. de 2021
 */
public class RatePlaceStress {

	private Place place;
	private Integer totalOccurrences;
	private Integer stressOccurrences;
	private Double ratio = null;
	private double[] probabilities;

	public RatePlaceStress() {
	}

	public RatePlaceStress(Place place, Integer totalOccurrences, Integer stressOccurrences) {
		this.place = place;
		this.totalOccurrences = totalOccurrences;
		this.stressOccurrences = stressOccurrences;
		
		if (stressOccurrences > 0 && totalOccurrences > 0) {
			this.ratio = stressOccurrences.doubleValue() / totalOccurrences.doubleValue();
		} else if (stressOccurrences == 0) {
			ratio = 0.0;
		} else if (totalOccurrences == 0) {
			ratio = 0.5;
		}
	}

	public Place getPlace() {
		return place;
	}

	public void setPlace(Place place) {
		this.place = place;
	}

	public Integer getTotalOccurrences() {
		return totalOccurrences;
	}

	public void setTotalOccurrences(Integer totalOccurrences) {
		this.totalOccurrences = totalOccurrences;
	}

	public Integer getStressOccurrences() {
		return stressOccurrences;
	}

	public void setStressOccurrences(Integer stressOccurrences) {
		this.stressOccurrences = stressOccurrences;
	}

	public Double getRatio() {
		return ratio;
	}

	public void setRatio(Double ratio) {
		this.ratio = ratio;
	}

	public double[] getProbabilities() {
		return probabilities;
	}

	public void setProbabilities(double[] probabilities) {
		this.probabilities = probabilities;
	}

}
