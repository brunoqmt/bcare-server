package br.unisinos.bcare.model.bean;

import java.util.Date;

/**
 * 
 * @author Bruno Mota Alves <bruno.alves@multi24h.com.br>
 * @since 5 de out. de 2021
 */
public class User {

	private Integer id;
	private String username;
	private Place homePoint = new Place();
	private Place workPoint = new Place();
	private Date workStartTime;
	private Date workEndTime;
	private Place studyPoint = new Place();
	private Date studyStartTime;
	private Date studyEndTime;
	private Place exclusionPlace = new Place();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Place getHomePoint() {
		return homePoint;
	}

	public void setHomePoint(Place homePoint) {
		this.homePoint = homePoint;
	}

	public Place getWorkPoint() {
		return workPoint;
	}

	public void setWorkPoint(Place workPoint) {
		this.workPoint = workPoint;
	}

	public Date getWorkStartTime() {
		return workStartTime;
	}

	public void setWorkStartTime(Date workStartTime) {
		this.workStartTime = workStartTime;
	}

	public Date getWorkEndTime() {
		return workEndTime;
	}

	public void setWorkEndTime(Date workEndTime) {
		this.workEndTime = workEndTime;
	}

	public Place getStudyPoint() {
		return studyPoint;
	}

	public void setStudyPoint(Place studyPoint) {
		this.studyPoint = studyPoint;
	}

	public Date getStudyStartTime() {
		return studyStartTime;
	}

	public void setStudyStartTime(Date studyStartTime) {
		this.studyStartTime = studyStartTime;
	}

	public Date getStudyEndTime() {
		return studyEndTime;
	}

	public void setStudyEndTime(Date studyEndTime) {
		this.studyEndTime = studyEndTime;
	}
	
	public Place getExclusionPlace() {
		return exclusionPlace;
	}

	public void setExclusionPlace(Place exclusionPlace) {
		this.exclusionPlace = exclusionPlace;
	}

	public void clearUser() {
		this.username = null;
		this.homePoint = null;
		this.workPoint = null;
		this.workStartTime = null;
		this.workEndTime = null;
		this.studyPoint = null;
		this.studyStartTime = null;
		this.studyEndTime = null;
		this.exclusionPlace = null;
	}

}
