package br.unisinos.bcare.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import br.unisinos.bcare.model.bean.TrailPrediction;

/**
 * 
 * @author Bruno Mota Alves <bruno.alves@multi24h.com.br>
 * @since  17 de out. de 2021
 */
public class TrailPredictionDAO {

	private Connection connection;

	public TrailPredictionDAO(Connection connection) {
		this.connection = connection;
	}

	/**
	 * Return a list of objects
	 * 
	 * @return List<TrailPrediction>
	 */
	public List<TrailPrediction> retrieveAll() {
		List<TrailPrediction> trailsPredictions = new ArrayList<>();
		PreparedStatement pstmt = null;
		try {
			String sql = "SELECT * FROM trailsxpredictions";
			pstmt = connection.prepareStatement(sql);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				TrailPrediction trailPrediction = new TrailPrediction();
				trailPrediction.setId(rs.getInt("id"));
				trailPrediction.setIdTrail(rs.getInt("id_trail"));
				trailPrediction.setIdAlgorithm(rs.getInt("id_algorithm"));
				trailPrediction.setRelaxedProbability(rs.getDouble("relaxed_probability"));
				trailPrediction.setNormalProbability(rs.getDouble("normal_probability"));
				trailPrediction.setStressedProbability(rs.getDouble("stressed_probability"));
				trailPrediction.setStatusPrediction(rs.getInt("status_prediction"));
				trailPrediction.setPredictedStress(rs.getBoolean("predicted_stress"));
				trailPrediction.setPredictionWasCorrect(rs.getBoolean("prediction_was_correct"));
				trailPrediction.setSourceModulePrediction(rs.getInt("source_module_prediction"));
				
				/* Add to list */
				trailsPredictions.add(trailPrediction);
			}
		} catch (Exception e) {
			System.out.println("#Error: Retrieve Trail Prediction. Message: " + e.getMessage());
			e.printStackTrace();
		}
		return trailsPredictions;
	}

	/**
	 * Return the match with ID
	 * 
	 * @param id
	 * @return TrailPrediction
	 */
	public TrailPrediction retrieveByID(Integer id) {
		TrailPrediction trailPrediction = new TrailPrediction();
		PreparedStatement pstmt = null;
		try {
			String sql = "SELECT * FROM trailsxpredictions WHERE id = ?";
			pstmt = connection.prepareStatement(sql);
			pstmt.setInt(1, id);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				trailPrediction.setId(rs.getInt("id"));
				trailPrediction.setIdTrail(rs.getInt("id_trail"));
				trailPrediction.setIdAlgorithm(rs.getInt("id_algorithm"));
				trailPrediction.setRelaxedProbability(rs.getDouble("relaxed_probability"));
				trailPrediction.setNormalProbability(rs.getDouble("normal_probability"));
				trailPrediction.setStressedProbability(rs.getDouble("stressed_probability"));
				trailPrediction.setStatusPrediction(rs.getInt("status_prediction"));
				trailPrediction.setPredictedStress(rs.getBoolean("predicted_stress"));
				trailPrediction.setPredictionWasCorrect(rs.getBoolean("prediction_was_correct"));
				trailPrediction.setSourceModulePrediction(rs.getInt("source_module_prediction"));

			}
		} catch (Exception e) {
			System.out.println("#Error: Retrieve Trail Prediction by ID. Message: " + e.getMessage());
			e.printStackTrace();
		}
		return trailPrediction;
	}

	/**
	 * Insert the Trail Prediction on Database.
	 * 
	 * @param connection
	 * @param user (to be inserted)
	 * @return ID generated OR null for fail cases
	 */
	public Integer insert(TrailPrediction trailPrediction) {
		String sql = "INSERT INTO trailsxpredictions(id_trail, id_algorithm, relaxed_probability, normal_probability, stressed_probability, status_prediction, predicted_stress, prediction_was_correct, source_module_prediction) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);";
		PreparedStatement pstmt;
		try {
			pstmt = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
			pstmt.setInt(1, trailPrediction.getIdTrail());
			pstmt.setInt(2, trailPrediction.getIdAlgorithm());
			pstmt.setDouble(3, trailPrediction.getRelaxedProbability());
			pstmt.setDouble(4, trailPrediction.getNormalProbability());
			pstmt.setDouble(5, trailPrediction.getStressedProbability());
			pstmt.setInt(6, trailPrediction.getStatusPrediction());
			pstmt.setBoolean(7, trailPrediction.getPredictedStress());
			pstmt.setBoolean(8, trailPrediction.getPredictionWasCorrect());
			pstmt.setInt(9, trailPrediction.getSourceModulePrediction());
			pstmt.execute();

			ResultSet rs = pstmt.getGeneratedKeys();
			rs.next();
			return rs.getInt(1);
		} catch (Exception e) {
			System.out.println("#Error: Inserting Trail Prediction. Message: " + e.getMessage());
			e.printStackTrace();
			return null;
		}
	}

}
