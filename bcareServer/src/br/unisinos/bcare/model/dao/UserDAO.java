package br.unisinos.bcare.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import br.unisinos.bcare.model.bean.User;

/**
 * 
 * @author Bruno Mota Alves <bruno.alves@multi24h.com.br>
 * @since 5 de out. de 2021
 */
public class UserDAO {

	private Connection connection;

	public UserDAO(Connection connection) {
		this.connection = connection;
	}

	/**
	 * Return a list of objects
	 * 
	 * @return List<User>
	 */
	public List<User> retrieveAll() {
		List<User> users = new ArrayList<>();
		PreparedStatement pstmt = null;
		try {
			String sql = "SELECT * FROM users";
			pstmt = connection.prepareStatement(sql);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				User user = new User();
				user.setId(rs.getInt("id"));
				user.setUsername(rs.getString("username"));
				user.getHomePoint().setId(rs.getInt("home_point"));
				user.getWorkPoint().setId(rs.getInt("work_point"));
				user.setWorkStartTime(rs.getTime("work_start_time"));
				user.setWorkEndTime(rs.getTime("work_end_time"));
				user.getStudyPoint().setId(rs.getInt("study_point"));
				user.setStudyStartTime(rs.getTime("study_start_time"));
				user.setStudyEndTime(rs.getTime("study_end_time"));
				user.getExclusionPlace().setId(rs.getInt("exclusion_place"));
				/* Add to list */
				users.add(user);
			}
		} catch (Exception e) {
			System.out.println("#Error: Retrieve users. Message: " + e.getMessage());
			e.printStackTrace();
		}
		return users;
	}

	/**
	 * Return the match with ID
	 * 
	 * @param id
	 * @return User
	 */
	public User retrieveByID(Integer id) {
		User user = new User();
		PreparedStatement pstmt = null;
		try {
			String sql = "SELECT * FROM users WHERE id = ?";
			pstmt = connection.prepareStatement(sql);
			pstmt.setInt(1, id);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				user.setId(rs.getInt("id"));
				user.setUsername(rs.getString("username"));
				user.getHomePoint().setId(rs.getInt("home_point"));
				user.getWorkPoint().setId(rs.getInt("work_point"));
				user.setWorkStartTime(rs.getTime("work_start_time"));
				user.setWorkEndTime(rs.getTime("work_end_time"));
				user.getStudyPoint().setId(rs.getInt("study_point"));
				user.setStudyStartTime(rs.getTime("study_start_time"));
				user.setStudyEndTime(rs.getTime("study_end_time"));
				user.getExclusionPlace().setId(rs.getInt("exclusion_place"));
			}
		} catch (Exception e) {
			System.out.println("#Error: Retrieve user by ID. Message: " + e.getMessage());
			e.printStackTrace();
		}
		return user;
	}

	/**
	 * Insert the User on Database.
	 * 
	 * @param connection
	 * @param user (to be inserted)
	 * @return ID generated OR null for fail cases
	 */
	public Integer insert(Connection connection, User place) {
		String sql = "INSERT INTO users(username, home_point, work_point, study_point, exclusion_place) VALUES (?, ?, ?, ?, ?);";
		PreparedStatement pstmt;
		try {
			pstmt = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, place.getUsername());
			pstmt.setInt(2, place.getHomePoint().getId());
			pstmt.setInt(3, place.getWorkPoint().getId());
			pstmt.setInt(4, place.getStudyPoint().getId());
			pstmt.setInt(5, place.getExclusionPlace().getId());
			pstmt.execute();

			ResultSet rs = pstmt.getGeneratedKeys();
			rs.next();
			return rs.getInt(1);
		} catch (Exception e) {
			System.out.println("#Error: Inserting User. Message: " + e.getMessage());
			return null;
		}
	}

	/**
	 * Delete the User using an ID
	 * @param connection
	 * @param id
	 * @return true/false
	 */
	public Boolean deleteByID(Connection connection, Integer id) {
		String sql = "DELETE FROM users WHERE id = ?;";
		PreparedStatement pstmt;
		try {
			pstmt = connection.prepareStatement(sql);
			pstmt.setInt(1, id);
			pstmt.execute();
			return true;
		} catch (Exception e) {
			System.out.println("#Error: Deleting the User by ID. Message: " + e.getMessage());
			return false;
		}
	}

}
