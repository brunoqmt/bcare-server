package br.unisinos.bcare.model.dao;

import java.sql.Connection;

/**
 * 
 * @author Bruno Mota Alves <bruno.alves@multi24h.com.br>
 * @since 7 de jul. de 2021
 */
public class RelatorioArvoreDAO {

	private Connection connection;

	public RelatorioArvoreDAO(Connection connection) {
		this.connection = connection;
	}

//	public List<RelatorioArvore> retrieveReports(Integer idModelo) {
//		List<RelatorioArvore> listReports = new ArrayList<>();
//		PreparedStatement pstmt;
//		try {
//			String sql = "SELECT * FROM solucoesglobais.rl_arvores ra WHERE ra.id_rl_modelos = ?";
//			pstmt = connection.prepareStatement(sql);
//			pstmt.setInt(1, idModelo);
//			ResultSet rs = pstmt.executeQuery();
//			while (rs.next()) {
//				RelatorioArvore relatorio = new RelatorioArvore();
//				relatorio.setId(rs.getInt("id"));
//				relatorio.setNome(rs.getString("nome"));
//				relatorio.setXml(rs.getBytes("xml"));
////            	relatorio.setCreated(rs.getInt("id"));
////            	relatorio.setModified(rs.getInt("id"));
//				relatorio.getModelo().setId(rs.getInt("id_rl_modelos"));
//				relatorio.setIdPai(rs.getInt("id_pai"));
//				listReports.add(relatorio);
//			}
//		} catch (Exception e) {
//			System.out.println("#Error: Retrieve fila_log. Message: " + e.getMessage());
//			e.printStackTrace();
//		}
//		return listReports;
//	}

}
