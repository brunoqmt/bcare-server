package br.unisinos.bcare.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import br.unisinos.bcare.model.bean.Place;

/**
 * 
 * @author Bruno Mota Alves <bruno.alves@multi24h.com.br>
 * @since 5 de out. de 2021
 */
public class PlaceDAO {

	private Connection connection;
	private CategoryTimeDAO categoryDAO;

	public PlaceDAO(Connection connection) {
		this.connection = connection;
		this.categoryDAO = new CategoryTimeDAO(connection);
	}

	/**
	 * Return a list of objects
	 * 
	 * @return List<Place>
	 */
	public List<Place> retrieveAll() {
		List<Place> places = new ArrayList<>();
		PreparedStatement pstmt = null;
		try {
			String sql = "SELECT * FROM places";
			pstmt = connection.prepareStatement(sql);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				Place place = new Place();
				place.setId(rs.getInt("id"));
				place.setDescription(rs.getString("description"));
				place.setLatitude(rs.getDouble("latitude"));
				place.setLongitude(rs.getDouble("longitude"));
				place.setCoefficient(rs.getDouble("coefficient"));
				place.getCategoryTime().setId(rs.getInt("id_category"));
				/* Get the category time complete */
				place.setCategoryTime(categoryDAO.retrieveByID(place.getCategoryTime().getId()));
				/* Add to list */
				places.add(place);
			}
		} catch (Exception e) {
			System.out.println("#Error: Retrieve places. Message: " + e.getMessage());
			e.printStackTrace();
		}
		return places;
	}

	/**
	 * Return the match with ID
	 * 
	 * @param id
	 * @return Place
	 */
	public Place retrieveByID(Integer id) {
		Place place = new Place();
		PreparedStatement pstmt = null;
		try {
			String sql = "SELECT * FROM places WHERE id = ?";
			pstmt = connection.prepareStatement(sql);
			pstmt.setInt(1, id);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				place.setId(rs.getInt("id"));
				place.setDescription(rs.getString("description"));
				place.setLatitude(rs.getDouble("latitude"));
				place.setLongitude(rs.getDouble("longitude"));
				place.setCoefficient(rs.getDouble("coefficient"));
				place.getCategoryTime().setId(rs.getInt("id_category"));
				
				/* Get the category time complete */
				place.setCategoryTime(categoryDAO.retrieveByID(place.getCategoryTime().getId()));
			}
		} catch (Exception e) {
			System.out.println("#Error: Retrieve place by ID. Message: " + e.getMessage());
			e.printStackTrace();
		}
		return place;
	}

	/**
	 * Insert the Place on Database.
	 * 
	 * @param connection
	 * @param place      (to be inserted)
	 * @return ID generated OR null for fail cases
	 */
	public Integer insert(Connection connection, Place place) {
		String sql = "INSERT INTO places(description, latitude, longitude, coefficient) VALUES (?, ?, ?, ?);";
		PreparedStatement pstmt;
		try {
			pstmt = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, place.getDescription());
			pstmt.setDouble(2, place.getLatitude());
			pstmt.setDouble(3, place.getLongitude());
			pstmt.setDouble(4, place.getCoefficient());
			pstmt.execute();

			ResultSet rs = pstmt.getGeneratedKeys();
			rs.next();
			return rs.getInt(1);
		} catch (Exception e) {
			System.out.println("#Error: Inserting Place. Message: " + e.getMessage());
			return null;
		}
	}

	/**
	 * Delete the Place using an ID
	 * @param connection
	 * @param id
	 * @return true/false
	 */
	public Boolean deleteByID(Connection connection, Integer id) {
		String sql = "DELETE FROM places WHERE id = ?;";
		PreparedStatement pstmt;
		try {
			pstmt = connection.prepareStatement(sql);
			pstmt.setInt(1, id);
			pstmt.execute();
			return true;
		} catch (Exception e) {
			System.out.println("#Error: Deleting the Place by ID. Message: " + e.getMessage());
			return false;
		}
	}

}
