package br.unisinos.bcare.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import br.unisinos.bcare.model.bean.CategoryTime;

/**
 * 
 * @author Bruno Mota Alves <bruno.alves@multi24h.com.br>
 * @since  11 de out. de 2021
 */
public class CategoryTimeDAO {
	
	private Connection connection;

	public CategoryTimeDAO(Connection connection) {
		this.connection = connection;
	}

	/**
	 * Return a list of objects
	 * 
	 * @return List<User>
	 */
	public List<CategoryTime> retrieveAll() {
		List<CategoryTime> users = new ArrayList<>();
		PreparedStatement pstmt = null;
		try {
			String sql = "SELECT * FROM category_time";
			pstmt = connection.prepareStatement(sql);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				CategoryTime categoryTime = new CategoryTime();
				categoryTime.setId(rs.getInt("id"));
				categoryTime.setDescription(rs.getString("description"));
				categoryTime.setSunday(rs.getBoolean("sunday"));
				categoryTime.setMonday(rs.getBoolean("monday"));
				categoryTime.setTuesday(rs.getBoolean("tuesday"));
				categoryTime.setWednesday(rs.getBoolean("wednesday"));
				categoryTime.setThursday(rs.getBoolean("thursday"));
				categoryTime.setFriday(rs.getBoolean("friday"));
				categoryTime.setSaturday(rs.getBoolean("saturday"));
				categoryTime.setOpeningTime(rs.getTime("opening_time"));
				categoryTime.setClosingTime(rs.getTime("closing_time"));
				/* Add to list */
				users.add(categoryTime);
			}
		} catch (Exception e) {
			System.out.println("#Error: Retrieve category times. Message: " + e.getMessage());
			e.printStackTrace();
		}
		return users;
	}

	/**
	 * Return the match with ID
	 * 
	 * @param id
	 * @return CategoryTime
	 */
	public CategoryTime retrieveByID(Integer id) {
		CategoryTime categoryTime = new CategoryTime();
		PreparedStatement pstmt = null;
		try {
			String sql = "SELECT * FROM category_time WHERE id = ?";
			pstmt = connection.prepareStatement(sql);
			pstmt.setInt(1, id);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				categoryTime.setId(rs.getInt("id"));
				categoryTime.setDescription(rs.getString("description"));
				categoryTime.setSunday(rs.getBoolean("sunday"));
				categoryTime.setMonday(rs.getBoolean("monday"));
				categoryTime.setTuesday(rs.getBoolean("tuesday"));
				categoryTime.setWednesday(rs.getBoolean("wednesday"));
				categoryTime.setThursday(rs.getBoolean("thursday"));
				categoryTime.setFriday(rs.getBoolean("friday"));
				categoryTime.setSaturday(rs.getBoolean("saturday"));
				categoryTime.setOpeningTime(rs.getTime("opening_time"));
				categoryTime.setClosingTime(rs.getTime("closing_time"));
			}
		} catch (Exception e) {
			System.out.println("#Error: Retrieve CategoryTime by ID. Message: " + e.getMessage());
			e.printStackTrace();
		}
		return categoryTime;
	}

}
