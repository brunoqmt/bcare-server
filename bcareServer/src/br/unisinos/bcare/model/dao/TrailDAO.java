package br.unisinos.bcare.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import br.unisinos.bcare.model.bean.Trail;

/**
 * 
 * @author Bruno Mota Alves <bruno.alves@multi24h.com.br>
 * @since 5 de out. de 2021
 */
public class TrailDAO {

	private Connection connection;

	public TrailDAO(Connection connection) {
		this.connection = connection;
	}
	
	public Boolean existsTrailHistory(Trail trail) {
		Integer count = null;
		PreparedStatement pstmt = null;
		try {
			String sql = "SELECT count(t.id) FROM trails t WHERE t.id_user = ? "
					+ "AND t.day_of_week = ? AND t.hour = ? AND t.id_place = ?";
			pstmt = connection.prepareStatement(sql);
			pstmt.setInt(1, trail.getUser().getId());
			pstmt.setString(2, trail.getDayOfWeek());
			pstmt.setString(3, trail.getHour());
			pstmt.setInt(4, trail.getPlace().getId());
			ResultSet rs = pstmt.executeQuery();
			
			rs.next();
			count = rs.getInt("count");
			
			if (count > 0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			System.out.println("#Error: Retrieve Trails. Message: " + e.getMessage());
			e.printStackTrace();
		}
		return false;
	}
	
	public Integer retrieveCountStressOccurrences(Integer idPlace, Integer idStressCondition) {
		Integer count = null;
		PreparedStatement pstmt = null;
		try {
			String sql = "SELECT count(t.id) FROM trails t WHERE t.id_place = ? AND t.id_stress_condition = ?";
			pstmt = connection.prepareStatement(sql);
			pstmt.setInt(1, idPlace);
			pstmt.setInt(2, idStressCondition);
			ResultSet rs = pstmt.executeQuery();
			
			rs.next();
			count = rs.getInt("count");
		} catch (Exception e) {
			System.out.println("#Error: Retrieve Trails. Message: " + e.getMessage());
			e.printStackTrace();
		}
		return count;
	}
	
	public Integer retrieveCountTrailOccurrences(Integer idPlace) {
		Integer count = null;
		PreparedStatement pstmt = null;
		try {
			String sql = "SELECT count(t.id) FROM trails t WHERE t.id_place = ?";
			pstmt = connection.prepareStatement(sql);
			pstmt.setInt(1, idPlace);
			ResultSet rs = pstmt.executeQuery();
			
			rs.next();
			count = rs.getInt("count");
		} catch (Exception e) {
			System.out.println("#Error: Retrieve Trails. Message: " + e.getMessage());
			e.printStackTrace();
		}
		return count;
	}

	/**
	 * Return a list of objects
	 * 
	 * @return List<Trail>
	 */
	public List<Trail> retrieveAll() {
		List<Trail> trails = new ArrayList<>();
		PreparedStatement pstmt = null;
		try {
			String sql = "SELECT * FROM trails";
			pstmt = connection.prepareStatement(sql);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				Trail trail = new Trail();
				trail.setId(rs.getInt("id"));
				trail.getUser().setId(rs.getInt("id_user"));
				trail.getPlace().setId(rs.getInt("id_place"));
				trail.setDatetimeMoment(rs.getDate("datetime_moment"));
				trail.setHour(rs.getString("hour"));
				trail.setStressCoef(rs.getDouble("stress_coef"));
				trail.getStressCondition().setId(rs.getInt("id_stress_condition"));
				trail.setDayOfWeek(rs.getString("day_of_week"));
				/* Add to list */
				trails.add(trail);
			}
		} catch (Exception e) {
			System.out.println("#Error: Retrieve Trails. Message: " + e.getMessage());
			e.printStackTrace();
		}
		return trails;
	}

	/**
	 * Return the match with ID
	 * 
	 * @param id
	 * @return Trail
	 */
	public Trail retrieveByID(Integer id) {
		Trail trail = new Trail();
		PreparedStatement pstmt = null;
		try {
			String sql = "SELECT * FROM trails WHERE id = ?";
			pstmt = connection.prepareStatement(sql);
			pstmt.setInt(1, id);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				trail.setId(rs.getInt("id"));
				trail.getUser().setId(rs.getInt("id_user"));
				trail.getPlace().setId(rs.getInt("id_place"));
				trail.setDatetimeMoment(rs.getDate("datetime_moment"));
				trail.setHour(rs.getString("hour"));
				trail.setStressCoef(rs.getDouble("stress_coef"));
				trail.getStressCondition().setId(rs.getInt("id_stress_condition"));
				trail.setDayOfWeek(rs.getString("day_of_week"));
			}
		} catch (Exception e) {
			System.out.println("#Error: Retrieve Trail by ID. Message: " + e.getMessage());
			e.printStackTrace();
		}
		return trail;
	}

	/**
	 * Insert the Trail on Database.
	 * 
	 * @param connection
	 * @param user (to be inserted)
	 * @return ID generated OR null for fail cases
	 */
	public Trail insert(Trail trail) {
		String sql = "INSERT INTO trails(id_user, id_place, datetime_moment, hour, stress_coef, id_stress_condition, day_of_week) VALUES (?, ?, ?, ?, ?, ?, ?);";
		PreparedStatement pstmt;
		try {
			pstmt = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
			pstmt.setDouble(1, trail.getUser().getId());
			pstmt.setDouble(2, trail.getPlace().getId());
			pstmt.setTimestamp(3, new Timestamp(trail.getDatetimeMoment().getTime()));
			pstmt.setString(4, trail.getHour());
			pstmt.setDouble(5, trail.getStressCoef());
			pstmt.setDouble(6, trail.getStressCondition().getId());
			pstmt.setString(7, trail.getDayOfWeek());
			pstmt.execute();

			ResultSet rs = pstmt.getGeneratedKeys();
			rs.next();
			trail.setId(rs.getInt(1));
			return trail;
		} catch (Exception e) {
			System.out.println("#Error: Inserting Trail. Message: " + e.getMessage());
			return null;
		}
	}

	/**
	 * Delete the Trail using an ID
	 * 
	 * @param connection
	 * @param id
	 * @return true/false
	 */
	public Boolean deleteByID(Integer id) {
		String sql = "DELETE FROM trails WHERE id = ?;";
		PreparedStatement pstmt;
		try {
			pstmt = connection.prepareStatement(sql);
			pstmt.setInt(1, id);
			pstmt.execute();
			return true;
		} catch (Exception e) {
			System.out.println("#Error: Deleting the Trail by ID. Message: " + e.getMessage());
			return false;
		}
	}

}
